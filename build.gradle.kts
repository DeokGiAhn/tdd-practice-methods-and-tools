import org.gradle.internal.impldep.org.eclipse.jgit.util.RawParseUtils.encoding
import org.gradle.internal.impldep.org.junit.platform.launcher.EngineFilter.includeEngines
import org.gradle.internal.impldep.org.junit.platform.launcher.TagFilter.excludeTags

plugins {
  `java-library`
  idea
  application
}

application {
  mainClassName = "xx.xx.xx.ApplicationInitializer"
  description = "This is a xxx Application."
  version = "2019.xx.0"
}

repositories {
  mavenLocal()
  maven {
    url = uri("https://nexus.wisoft.io/repository/maven-public/")
  }
  mavenCentral()
}

configure<JavaPluginConvention> {
  sourceCompatibility = JavaVersion.VERSION_11
  targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<JavaCompile> {
  options.apply {
    encoding = "UTF-8"
    compilerArgs = listOf("-Xlint:all", "-Xlint:-processing")
  }
}

tasks.withType<Wrapper> {
  gradleVersion = "5.2.1"
  distributionType = Wrapper.DistributionType.BIN
}

apply {
  from("gradle/module/main/all-deps.gradle.kts")
  from("gradle/module/test/all-deps.gradle.kts")
}
