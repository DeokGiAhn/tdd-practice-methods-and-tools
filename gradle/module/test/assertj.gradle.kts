object Versions {
  const val assertj = "3.12.0"
}

object Dependencies {
  const val assertj = "org.assertj:assertj-core:${Versions.assertj}"
}

val testImplementation by configurations
dependencies {
  testImplementation(Dependencies.assertj)
}
